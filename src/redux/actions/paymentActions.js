/* eslint-disable no-unused-vars */
import * as types from './actionTypes';
import * as paymentApi from '../../api/paymentApi';
import * as paymentStatusApi from '../../api/statusApi';
import { apiCallError } from './apiStatusAction';

export const loadPaymentListSuccess = (paymentList) =>{
    return{
        type: types.LOAD_PAYMENT_SUCCESS,
        payload: paymentList
    }
}

export const savePaymentSuccess = (saveResponse) =>{
    return{
        type: types.ADD_PAYEMENT_SUCCESS,
        payload: saveResponse
    }
}

export const loadPaymentStatusSuccess = (status) =>{
    return{
        type: types.GET_PAYMENT_STATUS_SUCCESS,
        payload: status
    }
}

export const loadPaymentFailure = () =>{
    return{
        type: types.LOAD_PAYMENT_FAILURE,
        payload:{message : "Error occured while processing the request."},
    }
}

export const loadPaymentStatusFailure = () =>{
    return{
        type: types.GET_PAYMENT_STATUS_FAILURE,
        payload:{message : "Error occured while processing the request."},
    }
}

export const beginApiCall = () =>{
    return{
        type: types.BEGIN_API_CALL,
    }
}

export const loadPaymentList = (searchParam) =>{
    return (dispatch, getState) =>{
        dispatch(beginApiCall());
        return paymentApi.getAllPayment(searchParam).then(paymentList => {
            dispatch(loadPaymentListSuccess(paymentList));
        }).catch(error => {
            dispatch(loadPaymentFailure());
        });
    }
}

export const savePayment = (payment) =>{
    return (dispatch, getState) =>{
        dispatch(beginApiCall());
        return paymentApi.savePayment(payment).then(saveRes => {
            dispatch(savePaymentSuccess(saveRes));
            return saveRes;
        }).catch(error => {
            dispatch(apiCallError(error));
            return error;
        });
    }
}

export const loadPaymentStatus = () =>{
    return (dispatch, getState) =>{
        dispatch(beginApiCall());
        return paymentStatusApi.getApiStatus().then(status => {
            dispatch(loadPaymentStatusSuccess(status));
        }).catch(error => {
            dispatch(loadPaymentStatusFailure());
        });
    }
}