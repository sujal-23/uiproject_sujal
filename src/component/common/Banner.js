import React from 'react';

const Banner =() => {
    return(
        <div className="jumbotron text-center">
  <h1 className="display-6" style={{color: "#007bff"}}>Payment App</h1>
  <p className="lead">This is a simple payment app, with attention to perform basic payment search by payment id and type.
  This app allow user to add/update payment related info.</p>
</div>
    )
}

export default Banner;