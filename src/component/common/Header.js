import React from 'React';
import { NavLink } from "react-router-dom";

const Header = () => {
    const activeStyle  = {color: '#F15B2A'}
    return(
<nav>
    
    <ul>
      <li className="nav-item active">
      <NavLink className="nav-link" to="/" activeStyle={activeStyle} exact >Home</NavLink> 
      </li>
      <li className="nav-item active">
      <NavLink activeStyle={activeStyle} className="nav-link" to="/about">About</NavLink> 
      </li>
      <li className="nav-item">
      <NavLink activeStyle={activeStyle} className="nav-link" to="/payment">Add Payment</NavLink>
      </li>
     
    </ul>
   
  
</nav>
    );
}

export default Header;