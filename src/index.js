
// import "bootstrap/dist/css/bootstrap.min.css";
// import "bootstrap/dist/js/bootstrap.bundle.min";
import 'jquery/src/jquery';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.bundle.min';
import './index.css';
import React from 'react';
import { render} from 'react-dom';
import App from './component/App';
import { BrowserRouter as Router } from 'react-router-dom';
import configureStore from './redux/configureStore';
import { Provider as ReduxProvider} from 'react-dedux';

const store = configureStore();

render(
  <ReduxProvider store= {store}>
<Router>
  <App />
</Router>
</ReduxProvider>,document.getElementById("app"));