/* eslint-disable react/prop-types */
import React from 'react';
import DisplayErrorMessage from '../common/DisplayErrorMessage';
import Spinner from '../common/Spinner';
import {Link} from 'react-router-dom';

const Payment = ({paymentList , loading, error}) =>
{
    const formatDate = (date) =>{
        return date.getDate() + "-"+ parseInt(date.getMonth()+1) +"-"+date.getFullYear() + " "+
         date.getHours() +":"+ date.getMinutes()+":" + date.getSeconds();
    }
    if (error) {
        
		return (
            <DisplayErrorMessage message={error.message} />
        );
	}

	if (loading) {
		return (
			<Spinner />
		);
    }
    
    return(
        <div className="container">
            <div className="text-center">
            <h4 style={{color: "#007bff"}}> Payment Details Section</h4>
            <br></br>
            </div>
            <div className="row">
                { paymentList.map((payment) => {
                    return(
                        <div key={payment.id} className="col-md-4" >
                        <div  className="card mb-4 box-shadow" style={{border: "1px solid rgb(234 185 125)",width: "20rem", wordBreak: "break-all"}}>
                                <div  className="card-header text-center">
                                    <h4  className="my-0 font-weight-primary" style={{color: "#007bff"}}> 
                                    <Link to = {"/payment/" + payment.id}>{ payment.id}</Link>
                                    </h4>
                                </div>
                                <div className="card-body">
                                 <h1 className="card-title pricing-card-title" style={{color: "rgb(234 185 125)"}}> ${payment.amount}</h1>
                                    <ul className="list-unstyled mt-3 mb-4">
                                        <li> <b className="fontDetPayment">Payment Type : </b> {payment.type}</li>
                                        <li> <b className="fontDetPayment">Payment Date : </b> { formatDate(new Date(payment.paymentdate ))}</li>
                                        <li> <b className="fontDetPayment">Customer Id : </b> {payment.custid}</li>
                                    </ul>
                             </div>
                        </div>
                        </div>
                        )
                    }
                )}
        </div>
       
       
            </div>
    );
}



export default Payment;
