/* eslint-disable react/no-unknown-property */
/* eslint-disable react/prop-types */
import React from "react";

const InputText = ({ type, name, textValue, title, onChange, error }) => {
  let wrapperClass = "form-group";
if(error && error.length > 0) {
  wrapperClass += " "+ "has-error";
}
  return (
    <div className={wrapperClass}>
      <label htmlFor={name}>{title}</label>
      <input  className="form-control form-group" type={type} id={name} value={textValue} onChange={onChange} />
      {error && <div className="alert alert-danger">{error} </div>}
  </div>
  );
};

export default InputText;
