import axios from 'axios';
import {handleResponse , handleError} from './apiUtils';
const baseUrl = process.env.API_URL + "status";

export function getApiStatus(){
    return axios.get(baseUrl).then(handleResponse).catch(handleError);
}