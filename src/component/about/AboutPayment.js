/* eslint-disable react/prop-types */
import React, { useEffect} from 'react';
import DisplayErrorMessage from '../common/DisplayErrorMessage';
import Spinner from '../common/Spinner';
import { connect } from 'react-redux';
import {loadPaymentStatus} from '../../redux/actions/paymentActions';

const AboutPayment = ({loadPaymentStatus, error, status, loading}) =>{

    useEffect(()=>{
        console.log("in about");
            loadPaymentStatus();
    
    }, []);

    if (error) {
		return (
            <DisplayErrorMessage message={error.message} />
        );
	}

	if (loading) {
		return (
			<Spinner />
		);
    }

    return(
        <div className="alert alert-danger" role="alert">
          {status}
        </div>
    )
}
const mapStateToProps = (state) => {
    return {
        status: state.payments.status,
        loading: state.payments.loading,
        error: state.payments.error,
    };
}

export default connect(mapStateToProps,{loadPaymentStatus})(AboutPayment);