import { combineReducers} from 'redux';
import  paymentReducer from './paymentReducer';
import apiCallStatusReducer from './apiStatusReducer'

const rootReducer = combineReducers({
    payments: paymentReducer,
    errors: apiCallStatusReducer 
});

export default rootReducer;