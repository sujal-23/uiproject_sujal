/* eslint-disable react/prop-types */
import React, {useEffect, useState } from 'react';
import Header from './common/Header';
import PaymentSearch from './search/PaymentSearch';
import Banner from './common/Banner';
import { Route, Switch } from 'react-router-dom';
import Payment from './payment/payment';
import {loadPaymentList} from '../redux/actions/paymentActions';
import { connect } from 'react-redux';
import AboutPayment from './about/AboutPayment';
import ManagePayment from './payment/ManagePayment';
import { ToastContainer} from 'react-toastify';
import "react-toastify/dist/ReactToastify.css";


const App = ({loadPaymentList, paymentList ,  loading, error}) =>
{
    const [searchParm, setSearchParam] = useState({searchText: "", searchType: "", isValid: false});
    //const [error, setError] = useState(error);
   
    useEffect(()=>{
           if(paymentList.length === 0 || searchParm.isValid){
            
            loadPaymentList(searchParm);
            
           }
    }, [searchParm,loadPaymentList]);

    return(
<div className="container my-4">
<Header />
<br></br>


<Switch>
    <Route exact path="/">
    <Banner />
    <PaymentSearch setSearch={setSearchParam} searchParm={searchParm}  />
    <Payment paymentList={paymentList}  loading={loading} error={error} />
    </Route>
    <Route path="/about">
    
        <AboutPayment />
        <Banner />
    </Route>
    <Route exact path="/payment/:id">
    <ManagePayment resetSearch={setSearchParam} searchParm={searchParm} />
    </Route>
    <Route   path="/payment">
    <ManagePayment resetSearch={setSearchParam} searchParm={searchParm}/>
    </Route>
  
  
</Switch>
<ToastContainer autoClose={3000} hideProgressBar />
</div>
    );
}

const mapStateToProps = (state) =>{
    console.log("state in pay", state);
    return {
        paymentList: state.payments.paymentList,
        loading: state.payments.loading,
        error: state.payments.error,
        status: state.payments.status,
    }
}

export default connect(mapStateToProps, { loadPaymentList })(App);