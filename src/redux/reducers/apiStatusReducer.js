import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function apiCallStatusReducer(state = initialState.error, action){
   
    if(action.type === types.API_CALL_ERROR){
        return {...state, error:action.payload};
    }
    else{
        return state;
    }
}