/* eslint-disable no-undef */
/* eslint-disable react/prop-types */
/* eslint-disable no-unused-vars */
import React, { useState} from 'react';
import AddPaymentForm from './AddPaymentForm';
import {savePayment} from '../../redux/actions/paymentActions';
import {connect} from 'react-dedux';
import '../common/initialState';

const ManagePayment2 =({savePayment, paymentList, props}) =>{

    // const[newpayment, setNewPayment] =useState({
    //     id:"",
    //     type:"",
    //     paymentdate:"",
    //     amount:null,
    //     custid:null
    // });
    console.log("in manage payment comp ");
    const[payment, setPayment] =useState(payment);
     
    // useEffect(()=> {
    //     console.log("in manage payment "+ loadPaymentList);
    //     console.log("in manage payment list "+  paymentList);
    //     console.log("in manage payment list "+  props);
         
    //        if(paymentList.length === 0)
    //        {
    //         loadPaymentList(props.searchParm);
    //        }
        
    // }, [props.paymentList]);


    const handleChange = (e) => {
        setPayment(prevPayment =>
        ({...prevPayment, [e.target.name]: e.target.value}));
    }

    const handleSubmit = (e) =>
    {
        e.PreventDefault();
        savePayment(payment);
    }

    return(
<div className="container my-4">
<AddPaymentForm handleSubmit={handleSubmit} handleChange={handleChange} payment={payment} />
</div>
    );
}

export function getPaymentById(paymentlist, id){
    return paymentlist.find(pay => pay.id === id) || null;
}

const mapStateToProps = (state, ownProps) =>{
    console.log("in manage payment state "+ state);
    const id = ownProps.match.params.id;
    console.log("in manage payment id "+ id +" "+ ownProps);
    const payment = id && state.payments.paymentList.length > 0
    ? getPaymentById(state.payments.paymentList, id) : newDefaultPayment;
    return {
        payment: payment,
        paymentList: state.payments.paymentList,
        error: state.errors.error,
        saveresult: state.saveresult,
    }
}


export default connect(mapStateToProps,{ savePayment})(ManagePayment2);