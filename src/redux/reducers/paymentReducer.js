import * as types from '../actions/actionTypes';
import initialState from './initialState';
export default function paymentReducer(state = initialState.paymentList, action){
    switch (action.type){
        case types.LOAD_PAYMENT_SUCCESS:
            return {...state, paymentList: action.payload, loading: false, error: null};
            case types.LOAD_PAYMENT_FAILURE:
                return {...state, paymentList: [], loading: false, error: action.payload, status: null};
        case types.BEGIN_API_CALL :
                return {...state,loading:true};
        case types.GET_PAYMENT_STATUS_SUCCESS:
                    return {...state,status: action.payload, error: null, loading: false};
        case types.GET_PAYMENT_STATUS_FAILURE:
                        return {...state, paymentList: [], status: null, error: action.payload, loading: false};
            case types.ADD_PAYEMENT_SUCCESS:
                return {...state, paymentList:[],  loading: false, error: null, saveresult: action.payload};
        default:
            return {...state}
    }
}