const webpack = require('webpack');
const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
//const { CleanWebpackPlugin } = require('clean-webpack-plugin');

process.env.NODE_ENV = 'development';
module.exports = {
    mode: 'development',
    entry: './src/index.js',
    output: {
        path: path.resolve(__dirname, "build"),
        filename: 'bundle.js',
        publicPath: '/',
    },
    devtool: 'inline-source-map',
    devServer:{
         contentBase: './build',
         port: 9000,
         //state: 'minimal',
        //overlay: true,
        historyApiFallback: true,
        //disableHostCheck: true,
        headers: { "Access-Control-Allow-Origin": "*"},
        //https:false
    },
    plugins:[
        
       new webpack.DefinePlugin({
           "process.env.API_URL": JSON.stringify("http://localhost:8080/api/")
       }),
        new HtmlWebpackPlugin({
           template: "src/index.html",
           favicon:"src/favicon-pay.ico",
            title: 'Development'
        })
    ],
    module:{
        rules:[
            {
                test:/\.(js|jsx)$/,
                exclude: /node_modules/,
                use: ["babel-loader", "eslint-loader"]
            },
            {
                test: /(\.css)$/,
                use:["style-loader", "css-loader"]
            }
        ]
    },
}