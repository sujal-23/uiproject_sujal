/* eslint-disable react/prop-types */
/* eslint-disable no-undef */
import React  from 'react';
import DateTimeInput from '../common/DateTimeInput';
import InputText from '../common/InputText';
import SelectInput from '../common/SelectInput';


const AddPaymentForm = ({handleSubmit, handleChange ,payment, errors}) =>{

    return(
        <>
<form onSubmit={handleSubmit}>
  <h2>{payment.id ? "Edit" : "Add" } Payment</h2>
  {errors.onSave && (
    <div className="alert alert-danger" role="alert">
      {errors.onSave}
    </div>
  )}
<InputText type="text" name="id" textValue= {payment.id} title= "Id" onChange={handleChange} error={errors.id} ></InputText>
<InputText type="text" name="type" textValue={payment.type} title= "Type" onChange={handleChange} error={errors.type} ></InputText>
<div className="form-group" style={{ display:"inline-flex"}}>
<DateTimeInput  name="paymentdate" textValue= {payment.paymentdate} title= "Payment Date" onChange={handleChange} error={errors.paymentdate} ></DateTimeInput>

<SelectInput  name="hh" textValue= {payment.hh} title= "HH" onChange={handleChange} max="24"></SelectInput>
<SelectInput  name="mm" textValue= {payment.mm} title= "MM" onChange={handleChange} max="59"></SelectInput>
<SelectInput  name="ss" textValue= {payment.ss} title= "SS" onChange={handleChange} max="59"></SelectInput>
</div>
<InputText type="number" name="amount" textValue={payment.amount} title= "Amount" onChange={handleChange} error={errors.amount}></InputText>
<InputText type="number" name="custid" textValue={payment.custid} title= "customer id" onChange={handleChange} error={errors.custid} ></InputText>
  <button type="submit" className="btn btn-primary">Submit</button>
</form>
</>
    );
}

export default AddPaymentForm;