/* eslint-disable react/prop-types */
import React from 'react';

const DisplayErrorMessage = ({message}) =>{
    return (
                <div className="alert alert-danger" role="alert">
                No records fetched, {message}
                </div>
    )
}

export default DisplayErrorMessage;