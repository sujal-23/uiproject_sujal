/* eslint-disable react/no-unknown-property */
/* eslint-disable react/prop-types */
import React from "react";

const DateTimeInput = ({  name, textValue, title, onChange}) => {
  return (
    <div className="form-group">
      <label htmlFor={name}>{title}</label>
      <input  className="form-control" type="date" id={name} value={textValue} onChange={onChange} />
  </div>
  );
};

export default DateTimeInput;
