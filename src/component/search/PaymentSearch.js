/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
import React, {useState}from 'react';
import {validateSearch} from '../common/Util.js';

const PaymentSearch = ({setSearch , searchParm , props}) => {

  const [errorMessage, setErrorMessage] = useState("");
  
const handleSubmit = () => {
  let searchparameter = {...searchParm};
  var msg = validateSearch(searchparameter.searchType,searchparameter.searchText);
  searchparameter = {...searchparameter, isValid: (msg)?false:true};
  setSearch(searchparameter);
  setErrorMessage(msg);
}

const handleReset = () =>{
  var msg = "";
  let searchparameter = {searchText: "", searchType: "", isValid: true};
  setSearch(searchparameter);
  setErrorMessage(msg);
}

  const onChange = (e) => {
     let searchparameter = {...searchParm, [e.target.name]: e.target.value, isValid: false};
     var msg = validateSearch(searchparameter.searchType,searchparameter.searchText);
     setErrorMessage(msg);
     setSearch(searchparameter);
  }



return(
    <>
    <hr className="my-4" />
    <div className="col-md-12">
                
                <div className="card-body">
<section className="search">
<nav className="navbar navbar-light bg-light">
    
<form className="form-inline col-md-12">
    <div className="col-md-3">
  <input className="form-control mr-sm-2" type="search" name="searchText" value={searchParm.searchText}  onChange={onChange} placeholder="Search" aria-label="Search" />
  </div>
  <div className="form-group col-md-3" style={{ display:"inline-flex"}}>
  <div className="col-md-4">
        <input type="radio" value="Id" name="searchType" checked ={searchParm.searchType === "Id"} onChange={onChange} /> Id
        </div>
        <div className="col-md-6">
        <input type="radio" value="Type" name="searchType" checked={searchParm.searchType === "Type"} onChange={onChange} /> Type
        </div>
      </div>
      <div className="col-md-2">
  <button className="btn btn-outline-primary" onClick={() => handleSubmit()} type="button">Search</button> 
  </div>
  <div className="col-md-1">
  <button className="btn btn-outline-primary" onClick={handleReset} type="button">Reset</button>
  </div>
</form>
</nav>
</section>
<br></br>
{(errorMessage) && <div className="alert alert-danger" role="alert">  {errorMessage}</div>}
<hr className="my-4" />

</div>
</div>
</>);
}

export default PaymentSearch;
