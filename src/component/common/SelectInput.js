/* eslint-disable react/no-unknown-property */
/* eslint-disable react/prop-types */
import React from "react";

const SelectInput = ({ name, textValue, title, onChange, max}) => {
  return (
    <div className="form-group col-md-3">
      <label htmlFor={name}>{title}</label>
      {
      <select  className="form-control" id={name} value={textValue} onChange={onChange} >
            {
            [...Array(Number(max)).keys()].map(i => {
              let keyval = i<10 ? '0'+i : i;
             return(
              <option key={keyval} value={keyval} > {keyval}</option>
             )      
             
            })
          
            }
        </select>
      }
          
  </div>
  );
};

export default SelectInput;
