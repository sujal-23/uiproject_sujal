export const validateNumber = (num) =>
{
   
    if(!num || isNaN(num)) { 
        return false;
    }

    return true;
}

export const validateTextField = (objVal) =>
{
    var isValid =false;
    if(objVal)
        {
        isValid = true;
        }
    
    
    return isValid;
}

export const validateSearch = (searchType, searchValue) =>
{
   
    if(validateTextField(searchType) && searchType == "Id" && !validateNumber(searchValue) )
    {
        return "Enter valid Id.";
    }
    else if(validateTextField(searchType) && searchType == "Type" && !validateTextField(searchValue) )
    {
        return "Enter a valid Type.";
    }
    else if(validateTextField(searchValue) && !validateTextField(searchType))
    {
        return "Select proper options.";
    }

    return null;
}