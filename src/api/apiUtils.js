export async function handleResponse(response){
    if(response.status === 200) 
    {
        if(Array.isArray(response.data))
        {
            return response.data;
        }
       
        return  [response.data];
    }
    if(response.status === 400){
        const res = await response.text();
        console.log("400 ", res);
        return { ...response, paymentlist: [], message: res};
    }

    return { ...response, paymentlist: [], message: "Network response was not ok."};
}

export function handleError(error) {
    console.log(error);
     throw "error occured";
}

export function setUrlParam(searchParam)
{
    var urlparam = "";
    if(searchParam.searchType == "Type")
    {
        urlparam = "paymentByType/"+searchParam.searchText;
    }
    else if(searchParam.searchType == "Id")
    {
        urlparam = "payment/"+searchParam.searchText;
    }
    else
    {
        urlparam = "all"
    }

    return urlparam;
}

export function formatDate(date){
    return date.getDate() + "/"+ parseInt(date.getMonth()+1)  +"/"+ date.getFullYear() ;
}