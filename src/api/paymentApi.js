import axios from 'axios';
import {handleResponse , handleError, setUrlParam, formatDate} from './apiUtils';
const baseUrl = process.env.API_URL;

export function getAllPayment(searchParam){
    return axios.get(baseUrl+setUrlParam(searchParam)).then(handleResponse).catch(handleError);
}

export function savePayment(payment){
    let dateValue = formatDate(new Date(payment.paymentdate)) + " "+ payment.hh +":" + payment.mm +":" + payment.ss;
    payment = {...payment, paymentdate:dateValue};
    return axios.post(baseUrl+"save", payment).then(handleResponse).catch(handleError);
}