/* eslint-disable react/prop-types */
import React, { useState, useEffect} from 'react';
import AddPaymentForm from './AddPaymentForm';
import {connect} from 'react-redux';
import {savePayment} from '../../redux/actions/paymentActions';
import Spinner from '../common/Spinner';
import {useParams, useHistory} from 'react-router-dom';
import {toast} from 'react-toastify';

const ManagePayment = ({loadPaymentList, savePayment, paymentList, loading,resetSearch, ...props}) =>{

    const { id } = useParams();
    const history = useHistory();
    const [errors, setErrors] = useState({});
    const [payment,setPayment] = useState({
            id:"",
            type:"",
            paymentdate:"",
            amount:"",
            custid:"",
            hh:"00",
            mm:"00",
            ss:"00"
        });

        const formatDate = (date) =>{
            return date.getFullYear() + "-"+ parseInt(date.getMonth()+1)  +"-"+ date.getDate() ;
        }

     const getPaymentById = (paymentlist, id) =>{
            return paymentlist.find(pay => pay.id == id);
        }

 useEffect(()=> {
    
    if(paymentList.Length == 0)
    {
        loadPaymentList(props.searchParm);
    }
    if( id && paymentList.length > 0)
    {
        const paymententity = getPaymentById(paymentList, id);
       if(paymententity)
       {
        let newDate = new Date(paymententity.paymentdate);
        setPayment({...paymententity,
             paymentdate: formatDate(newDate),
             hh: newDate.getHours() < 10 ? "0"+newDate.getHours() : newDate.getHours(),
             mm: newDate.getMinutes() < 10 ? "0"+newDate.getMinutes(): newDate.getMinutes(),
             ss:newDate.getSeconds() < 10 ? "0"+newDate.getSeconds(): newDate.getSeconds()
            });
        }
    }
     
        
 }, [id,paymentList]);
         

    const onChange = ({target}) => {
        setPayment({...payment, [target.id]: target.value});
    }

    function formValidate(){
        const {id, type, paymentdate, amount, custid} = payment;
        const errors = {};

        if(!id ) errors.id = "Id is required.";
        if(!type) errors.type = "Type is required.";
        if(!paymentdate) errors.paymentdate = "payment Date is required.";
        if(!amount) errors.amount = "Amount is required.";
        if(!custid) errors.custid = "Customer Id is required.";

        setErrors(errors);

        return Object.keys(errors).length === 0;
    }

    const handleSubmit = (e) =>
    {
        e.preventDefault();
        if(!formValidate()) return;
        savePayment(payment).then( (res) => {
            if(!res.includes("error"))
            {
                resetSearch({...props.searchParm,searchText: "", searchType: "", isValid: false});
                toast.success("Record Saved Successfully!!");
                history.push("/");
            }
            else
            {
                setErrors({onSave: "Error Occured on Save,please try after some time."});
            }
               
        }).catch(err => {
            setErrors({onSave: "Error Occured on Save" + err})
        });
    }
    

if(loading)
{
    return(
        <Spinner />
    )
}
    return(
        
            <div className="col-md-7" style={{border: "1px solid rgb(234 185 125)"}}>
                
                 <div className="card-body">
    <AddPaymentForm handleSubmit={handleSubmit} handleChange={onChange} payment={payment} errors = {errors} />
    </div>
 
    </div>
    );
}



const mapStateToProps = (state) => {
    return {
        paymentList: state.payments.paymentList,
    }
}

export default connect(mapStateToProps,{ savePayment})(ManagePayment);